FROM openjdk:11-jdk

WORKDIR /app

COPY gradlew gradlew.bat settings.gradle ./

COPY src ./src

RUN ./gradlew build

EXPOSE 8080

CMD ["java", "-jar", "build/libs/your-app.jar"]
